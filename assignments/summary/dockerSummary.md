# Docker Basics
* ## Docker
 Docker is an open source project that makes it easy to create containers and container-based apps.It is a set of   platform-as-a-service products that create isolated virtualized environments for building, deploying, and testing applications.
 
 ![](extras/docker_logo.png)

 ## Docker Terminologies
 ### 1. Docker Image
  * A Docker image is an immutable (unchangeable) file that contains the source code, libraries, dependencies, tools, and other files needed for an application to run.
  * A Docker image is contains everything needed to run an applications as a container. This includes: code, runtime, libraries, environment variables, configuration files
* The image can then be deployed to any Docker environment and as a container.
 ### 2. Docker container
 * A Docker container is a virtualized run-time environment where users can isolate applications from the underlying system. A container is, ultimately, just a running image. 
 * A Docker container is a running Docker image.
 * From one image you can create multiple containers .

![Docker container](extras/doc_contnr.png)

 ### How do I run a docker container?
 * Follow these steps:
  1. Use `docker ps` to get the name of the existing container.
  2. Use the command `docker exec -it <container name> /bin/bash` to get a bash shell in the container.
  3. Or directly use `docker exec -it <container name> <command>` to execute whatever command you specify in the container.

 ### 3. Docker Hub
  * Docker Hub is a SaaS repository for sharing and managing containers, where you will find official Docker images from open-source projects and software vendors and unofficial images from the general public. 
  * Docker Hub is like GitHub but for docker images and containers.
 ### 4. Docker Files
  * A Dockerfile is a simple text file that contains a list of commands the Docker client calls (on the command line) when assembling an image. 
 ### 5. Docker Registries
  * A Docker registry stores Docker images.
 ### 6. Docker Engine
Docker Engine is a client-server application with these major components:
 * A server which is a type of long-running program called a daemon process (the dockerd command).
 * A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.
 * A command line interface (CLI) client (the docker command).

 ![Docker Engine Components Flow](extras/engine-components-flow.png)

## Docker Architecture
 * Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

 The docker architecture is illustrated in the following figure:  

 ![architecture](https://docs.docker.com/engine/images/architecture.svg)
## Docker workflow
 Step-by-step workflow for developing Docker containerized apps: 
 1. Locally build and test a Docker image on your development box.
 2. Build your official image for testing and deployment.
 3. Deploy your Docker image to your server.
 
 ![img](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)

## Docker Common Operations
 1. Downloading/pulling the docker images wanted to be worked with.
 2. Copying code inside the docker
 3. Accessing docker terminal
 4. Installing additional required dependencies
 5. Compile and Run the Code inside docker
 6. Document steps to run program in README.md file
 7. Commit the changes done to the docker.
 8. Push docker image to the docker-hub and share repository with people who want to try the code.

## Install Docker on your machine
For Ubuntu:

 First, update your packages:
 >$ sudo apt update

 Next, install docker with apt-get:
 >$ sudo apt install docker.io

 Finally, verify that Docker is installed correctly:
 >$ sudo docker run hello-world

## Docker Basic Commands
 Learning basic docker commands

 * ## docker ps
   The docker ps command allows us to view all the containers that are running on the Docker Host.
 
 for Example:
 <pre class="code highlight js-syntax-highlight shell" lang="shell" v-pre="true"><code><span id="LC1" class="line" lang="shell">$ docker ps

 CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
 30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute                 elated_franklin</span></code></pre>
* ## docker start
 This command starts any stopped container(s).
 
 Example
 >$ docker start 30986

 In the above example, Docker starts the container beginning with the container ID 30986.
 
 >$ docker start sarita
 
 Whereas in this example, Docker starts the container named sarita.

* ## docker stop
 This command stops any running container(s).
 
 Example
 >$ docker stop 30986
 
 In the above example, Docker will stop the container beginning with the container ID 30986.
 
 >$ docker stop sarita
 
 Whereas in this example, Docker will stop the container named sarita.

* ## docker run
 This command creates containers from docker images.
 Example
 > $ docker run srita

* ## docker rm
 This command deletes the containers.

 Example
 > $ docker rm sarita

## More Docker Commands

 ### For Containers

 Use `docker container my_command`

 `create` — Create a container from an image.

 `start` — Start an existing container.

 `run` — Create a new container and start it.

 `ls` — List running containers.

 `inspect` — See lots of info about a container.

 `logs` — Print logs.

 `stop` — Gracefully stop running container.

 `kill` —Stop main process in container abruptly.

 `rm`— Delete a stopped container.

 ### For Images

 Use `docker image my_command`

 `build` — Build an image.

 `push` — Push an image to a remote registry.

 `ls` — List images.

 `history` — See intermediate image info.

 `inspect` — See lots of info about an image, including the layers.

 `rm` — Delete an image.

 ### For Misc

 `docker version` — List info about your Docker Client and Server versions.

 `docker login` — Log in to a Docker registry.

 `docker system prune` — Delete all unused containers, unused networks, and dangling images.