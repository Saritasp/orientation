# **Git Basics**
## Git
 ![](https://www.linuxjournal.com/sites/default/files/styles/360_250/public/nodeimage/story/git-icon.png?itok=w7zB9vuE)
## What is Git?
 "Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency". 

 The advantages of Git over other source control systems are:
 * Free and open source
 * Distributed
 * Data assurance
 * Staging area
 * Branching and merging

## Git Features
 ![](https://image.slidesharecdn.com/whatisgit-devopstutorial-edureka-170525072621/95/what-is-git-what-is-github-git-tutorial-github-tutorial-devops-tutorial-edureka-21-638.jpg?cb=1495698093)
 * ### Distributed System
  Distributed systems are those which allow the users to perform work on a project from all over the world. A distributed system holds a Central repository that can be accessed by many remote collaborators by using a Version Control System.
  ![img](https://media.geeksforgeeks.org/wp-content/uploads/20191203164948/Distributed-Version-Control-System.jpg)
 * ### Compatibility
  Git is compatible with all the Operating Systems that are being used these days. Git repositories can also access the repositories of other Version Control Systems like SVN, CVK, etc.
 * ### Non-linear Development
  Git allows users from all over the world to perform operations on a project remotely. A user can pick up any part of the project and do the required operation and then further update the project.
 * ### Open-Source
  Git is a free and open-source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. It is called open-source because it provides the flexibility to modify its source code according to the user’s needs. 

## Git Terminologies
 * ### Repository
  Git repository is a directory that stores all the files, folders, and content needed for your project.  
 * ### Remote 
  It is a shared repository that all team members use to exchange their changes.
 * ### Master
  The primary branch of all repositories. All committed and accepted changes should be on the master branch.
 * ### Branch
  A version of the repository that diverges from the main working project or master branch.
 * ### Clone
  A clone is a copy of a repository or the action of copying a repository.
 * ### Fetch
  By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.
 * ### Fork
  Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.  
 * ### Merge
  Taking the changes from one branch and adding them into another (traditionally master) branch.
 * ### Push
  Updates a remote branch with the commits made to the current branch. 
 * ### Pull
  If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request.

  ![git](https://www.earthdatascience.org/images/earth-analytics/git-version-control/git-fork-clone-flow.png)
  

## Git Workflow
 #### Workflow of Git
   ![Workflow of Git](https://support.cades.ornl.gov/user-documentation/_book/contributing/screenshots/git-workflow-steps.png)
   

 * Git has remote version where teams can collaborate remotely these services are offered by github,gitlab etc.

#### Git Workflow  
 * What is a Repository?  
 A repository a.k.a. **repo** is a collection of source code.  
 
 **There are four fundamental elements in the Git Workflow.**  
 1. Working Directory
 2. Staging Area
 3. Local Repository
 4. Remote Repository


 ![GitWorkflow](extras/GitWorkflow.png)

 If you consider a file in your Working Directory,it can be in three possible states.  
 1. **It can be staged**: Which means the files with the updated changes are marked to be committed to the local repository but not yet committed.  
 2. **It can be modified**: Which means the files with the updates changes are not yet stored in the local repository.  
 3. **It can be committed**: Which means that the changes you made to your file are safely stored in the local repository.

 **The following actions are the steps of Git workflow:**

 1. Clone the repository
 2. git fetch and git rebase
 3. Create a new branch
 4. Add commits
 5. git fetch and git rebase (on the master branch)
 6. Push the commits
 7. Create a Pull Request
 8. Discuss, review and merge pull request

 It can be explained through a flowchart:

 ![GitWorkflow](extras/Git_workflow.png)

## Basic Git Commands

 ### Commands
 * **Initialize a new Git repository: Git init**  
Everything you code is tracked in the repository. To initialize a git repo, use this command while inside the project folder. This will create a .git folder.  
 > git init

 * **Git add**  
 This command adds one or all changes files to the staging area.  

 1. To add just a specific file to staging:  
 > git add filename.py  
 2. To stage new, modofied or deleted files:
 > git add -A
 3. To stage new and modified files:
 > git add . 
 4. To stage modified and deleted files:
 > git add -u

 * **Git commit**
 This command records the file in the version history. The -m means that a commit message follows. This meassage ia a custom one and you should use it to refer in future.
 > git commit -m "your text"

 * **Git status**
 This command will list files in green or red colors. Green files have been added to the stage but not committed yet. Files marked as red are the ones not yet added to the stage.
 >git status

* **Working with Branches**
 1. This will create a new branch:
 > git branch branch_name
 2. To switch from one branch to another:
 > git checkout branch_name
 3. To create new branch and switch to it automatically:
 > git checkout -b branch_name
 4. To list all branches and see on what branch you currently are:
 > git branch
 5. This command will list the version history for the current branch:
 > git log

* **Push and Pull**
 1. This command sends the committed changes to remote repo:
 > git push
 2. To pull the changes from the remote server to your local computer:
 > git pull

* **Tips and Tricks**
 1. Throw away all you uncommitted changes:
 > git reset --hard
 2. Edit a commit message:
 > git commit --amend -m "New Message"
 3. Remove file from git without removing it from computer
 > git reset file_name
 4. make git ignore a file from adding to git:
 > echo file_name >> .gitignore

 ![image](https://i0.wp.com/build5nines.com/wp-content/uploads/2020/04/Git-Cheat-Sheet-Build5Nines.jpg?fit=787%2C506&ssl=1)

